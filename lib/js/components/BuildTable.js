import React from 'react';

const createRows = (data) => {
    // Loop through each data entry and return row values
    const elements = [];
    for (let i = 0; i < data.length; i += 1) {
        const currentData = data[i];
        elements.push(
            <div className="row" key={`${currentData.sku}-${currentData.advertiser_id}`}>
                <div className="data-cell">{currentData.product_name}</div>
                <div className="data-cell">{currentData.sku}</div>
                <div className="data-cell">{currentData.advertiser_name}</div>
                <div className="data-cell">{currentData.advertiser_id}</div>
            </div>
        );
    }
    if (elements.length) { return elements; } else {
        return 'No results found, try a different search.'
    }
}

const BuildTable = ( {data} ) => {
    return(
        <div id="main-table">
            <div className="header row">
                <div className="data-cell">Product Name <span className="filter-icon">&laquo;</span></div>
                <div className="data-cell">Product SKU <span className="filter-icon">&laquo;</span></div>
                <div className="data-cell">Advertiser Name <span className="filter-icon">&laquo;</span></div>
                <div className="data-cell">Advertiser ID <span className="filter-icon active">&laquo;</span></div>
            </div>
            {createRows(data)}
        </div>
    )
}
export default BuildTable;