import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import BuildTable from './BuildTable';
import getData from '../modules/getData';
import Search from './Search';

export default class Rakuten extends Component {

    static propTypes = {}

    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            tableData: [],
            currentPage: 0,
            tableCount: 0,
            currentSearch: 'http://localhost:8080/api',
        };
    }

    componentWillMount() {
        this.initialDataCall();
        this.initialDataCount();
    }

    updateCurrentSearch = (url) => {
        this.setState({
            currentSearch: url,
        });
    }

    updateTableData = (newTableData, reset, url) => {
        let finalTableData;
        if (reset) {
            finalTableData = JSON.parse(newTableData);
            this.updateCurrentSearch(url);
            this.initialDataCount(url);
        } else {
            finalTableData = JSON.parse(JSON.stringify(this.state.tableData));
            JSON.parse(newTableData).map((entry) => {
                finalTableData.push(entry);
            });
        }
        this.setState({
            tableData: finalTableData,
        });
    }

    updateTableCount = (newTableCount) => {
        this.setState({
            tableCount: newTableCount,
        });
    }

    updateCurrentPage = (newPage) => {
        this.setState({
            currentPage: newPage,
        });
    }

    initialDataCount = (existingURL) => {
        const url = existingURL ? `${existingURL}&count=true` : `http://localhost:8080/api?count=true`;
        getData(url).then(data => {
            const fixData = JSON.parse(data)[0]['COUNT(*)'];
            this.updateTableCount(fixData);
        }).catch((error) => {
            console.warn(`getData() Error: ${error}`);
        });
    }

    initialDataCall = () => {
        const url = 'http://localhost:8080/api';
        getData(url).then(data => {
            this.updateTableData(data);
        }).catch((error) => {
            console.warn(`getData() Error: ${error}`);
        });
    }

    render() {
        return (
            <div className="rakuten-table">
                <Search 
                    updateTableData={this.updateTableData}
                    page={this.state.currentPage}
                    updateCurrentPage={this.updateCurrentPage}
                    tableCount={this.state.tableCount}
                    currentSearch={this.state.currentSearch}
                />
                <BuildTable
                    data={this.state.tableData}
                />
            </div>
        );
    }
}
