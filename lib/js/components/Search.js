import React from 'react';
import getData from '../modules/getData';

const loadMoreProducts = (page, updateData, updatePage, currentSearch) => {
    const newPage = page + 1;
    const url = currentSearch.indexOf('?') > -1 ? `${currentSearch}&page=${newPage}` : `${currentSearch}?page=${newPage}` ; 
        getData(url).then(data => {
            updatePage(newPage);
            updateData(data);
        }).catch((error) => {
            console.warn(`loadMoreProducts() Error: ${error}`);
        });
}

const loadNewProducts = (url, updateData, updatePage) => {
        getData(url).then(data => {
            updatePage(0);
            updateData(data, true, url);
        }).catch((error) => {
            console.warn(`loadNewProducts() Error: ${error}`);
        });
}

let searchData = {};

const onSubmit = (event, updateData, updatePage) => {
    event.preventDefault();
    const value = searchData.input.value;
    const type = searchData.type.value;
    if (value && type) {
        const url = `http://localhost:8080/api?${type}=${value}`;
        loadNewProducts(url, updateData, updatePage);
    }
    // onChange(cityInput.value)
}

const Search = ({page, updateTableData, updateCurrentPage, tableCount, currentSearch}) => {
    return(
        <div className="search">
            <div className="current-search search-section">
                <span>Current Search: "{currentSearch}"</span>
            </div>
            <div className="search-count search-section">
                Showing <span className="result-count">{((page + 1) * 50) < tableCount ? `1-${(page + 1) * 50}` : tableCount}</span> results out of <span className="result-count">{tableCount}</span>&nbsp;
                {((page + 1) * 50) < tableCount &&
                    <button
                    className="action-button"
                    onClick={(event => {
                        loadMoreProducts(page, updateTableData, updateCurrentPage, currentSearch)
                    })}
                    >
                        Load More
                    </button>
                }
            </div>
            <div className="search-function search-section">
                Server Lookup:&nbsp;
                <form
                    className="search-form"
                    onSubmit={(event) => {onSubmit(event, updateTableData, updateCurrentPage)}}
                >
                    <select type="dropdown" ref={ element => searchData.type = element }>
                        <option value="product_name" name="product-name">Product Name</option>
                        <option value="sku" name="product-sku">Product SKU</option>
                        <option value="advertiser_name" name="advertiser-name">Advertiser Name</option>
                        <option value="advertiser_id" name="advertiser-name">Advertiser ID</option>
                    </select>&nbsp;
                    <input
                        type="text"
                        placeholder="Enter search terms..."
                        ref={ element => searchData.input = element }
                    />&nbsp;
                    <button className="action-button">
                        Search
                    </button>
                </form>
            </div>
        </div>
    )
}

export default Search;