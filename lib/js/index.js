import React from 'react';
import ReactDOM from 'react-dom';
import Rakuten from './components/Rakuten';
import styles from '../less/styles.less';

// Make sure os variable exists, if not just create it (for pages where it never gets built)
document.addEventListener('DOMContentLoaded', () => {
    const rootSelector = document.getElementById('app');
    ReactDOM.render(
        <Rakuten />, rootSelector
    );
});
