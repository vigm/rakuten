const checkFolders = require('./modules/checkFolders');
const checkSeedFiles = require('./modules/checkSeedFiles');
const setupDatabase = require('./modules/setupDatabase.js');
// Create folders if needed
checkFolders();
// Get Seed Data from Server if needed
checkSeedFiles().then(() => {setupDatabase()});
