const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const Products = require('../app/models/Products');

// use bodyParser() to get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Set port
const port = process.env.PORT || 8080;

// Create express Router
const router = express.Router();

// middleware to use for all requests
router.use((req, res, next) => {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3600');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    next(); // make sure we go to the next routes and don't stop here
});

router.get('/:page?/:count?/:advertiser_id?', (req, res, next) => {
    // Get requested page
    const page = req.query.page || 0;
    const count = req.query.count || false;
    const advertiser_id = req.query.advertiser_id || false;
    const advertiser_name = req.query.advertiser_name || false;
    const sku = req.query.sku || false;
    const product_name = req.query.product_name || false;
    if (advertiser_id) {
        Products.getProductsByAdvertiserID(advertiser_id, page, count, (err, rows) => {
            if(err) { res.json(err); } else { res.json(rows); }
        });
    } else if (advertiser_name) {
        Products.getProductsByAdvertiserName(advertiser_name, page, count, (err, rows) => {
            if(err) { res.json(err); } else { res.json(rows); }
        });
    } else if (sku) {
        Products.getProductsByProductSKU(sku, page, count, (err, rows) => {
            if(err) { res.json(err); } else { res.json(rows); }
        });
    } else if (product_name) {
        Products.getProductsByProductName(product_name, page, count, (err, rows) => {
            if(err) { res.json(err); } else { res.json(rows); }
        });
    } else {
        Products.getAllProducts(page, count, (err, rows) => {
            if(err) { res.json(err); } else { res.json(rows); }
        });
    }
});

// Register routes
app.use('/api', router);

// Start server
app.listen(port);
console.log('Magic happens on port ' + port);
