var targz = require('targz');
const config = require('../config');

const decompress = () => {
    console.log(`Attempting to decompress ${config.files.compressed}`);
    // decompress files from tar.gz archive 
    targz.decompress({
        src: `${config.folders.seed}/${config.files.compressed}`,
        dest: config.folders.decompressed,
    }, error => {
        if (error) { console.log(error); } else {
            console.log(`Finished decompressing ${config.files.compressed} to ${config.folders.decompressed}`);
            // digestCSV();
        }
    });
}

module.exports = decompress;