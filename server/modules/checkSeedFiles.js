const fs = require('fs');
const https = require('https');
const config = require('../config');
const decompress = require('./decompressSeedFiles');
var targz = require('targz');

const checkSeedFiles = () => {
    return new Promise((resolve, reject) => {
        const decompress = () => {
            console.log(`Attempting to decompress ${config.files.compressed}`);
            // decompress files from tar.gz archive 
            targz.decompress({
                src: `${config.folders.seed}/${config.files.compressed}`,
                dest: config.folders.decompressed,
            }, error => {
                if (error) { console.log(error); } else {
                    console.log(`Finished decompressing ${config.files.compressed} to ${config.folders.decompressed}`);
                    resolve();
                }
            });
        }
        
        const retrieveFile = () => {
            console.log(`Attempting to retrieve data from '${config.seedURL}'`);
            // Download the file
            const request = https.get(config.seedURL, response => {
                console.log('Successfully retrieved data');
                // Write file to folder
                console.log(`Attempting to send data to folder: ${config.folders.seed}/${config.files.compressed}`);
                response.pipe(
                    fs.createWriteStream(`${config.folders.seed}/${config.files.compressed}`)
                );
            }).on('close', () => { // When finished writing to folder
                console.log(`Successfully sent '${config.files.compressed}' to folder: '${config.folders.seed}'`);
                // Decompress the gzipped file
                decompress();
            });
        }
    
        // Check for files
        const fileCheck = (filePath) => {
            // Does the file exist
            if (fs.existsSync(filePath)) {
                console.log(`'${filePath}' file already exists`);
                decompress();
            } else {
                console.log(`'${filePath}' file doesn't exist!`);
                retrieveFile();
            }
        }
    
        console.log(`Checking for ${config.files.compressed} file`);
        fileCheck(`${config.folders.seed}/${config.files.compressed}`);
    })
}

module.exports = checkSeedFiles;