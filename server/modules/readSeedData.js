const config = require('../config');
const fs = require('fs');
var csvParser = require('csv-parse');

const readSeedData = () => {
    return new Promise((resolve, reject) => {
        console.log(`Attempting to read seed data from '${config.folders.decompressed}/${config.files.products}'`);
        var filePath = `${config.folders.decompressed}/${config.files.products}`;
        fs.readFile(filePath, {encoding: 'utf-8'}, function(error, csvData) {
            if (error) {
                console.log(`Error attempting to read file: ${error}`);
            }
            csvParser(csvData,
                {
                    relax_column_count: true,
                    delimiter: ',',
                    trim: true,
                    rtrim: true,
                    ltrim: true,
                    skip_empty_lines: true,
                    skip_lines_with_empty_values: true,
                },
                function(error, data) {
                    if (error) {
                        console.log(`Error attempting to read file: ${error}`);
                    } else {
                        console.log(`Successfully read ${config.files.products} data`);
                        resolve(data);
                    }
            });
        });
    })
};

module.exports = readSeedData;