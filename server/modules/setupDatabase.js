var mysql = require('mysql');
const fs = require('fs');
const config = require('../config');
const readSeedData = require('./readSeedData');
const ProgressBar = require('progress');

const setupDatabase = () => {
    // Connection 1 - Just to local server, status of database unknown
    const con1 = mysql.createConnection({
        host: config.mysql.host,
        user: config.mysql.user,
        password: config.mysql.password,
    });
    // Connection 2 - Just to local server, database known
    const con2 = mysql.createConnection({
        host: config.mysql.host,
        user: config.mysql.user,
        password: config.mysql.password,
        database: config.mysql.database,
    });
    
    const checkDatabase = (dbName) => {
        console.log(`Checking for existing '${dbName}' database`);
        return new Promise((resolve, reject) => {
            con1.query(`SHOW DATABASES LIKE '${dbName}'`, function (error, result) {
                // Check for errors while looking up
                if (error) throw error;
                // Return any results
                var answer = (result.length > 0) ? true : false;
                resolve(answer);
            });
        });
    }

    const checkTable = (tblName) => {
        console.log(`Checking for existing '${tblName}' table`);
        return new Promise((resolve, reject) => {
            con2.query(`SHOW TABLES LIKE '${tblName}'`, function (error, result) {
                // Check for errors while looking up
                if (error) throw error;
                // Return any results
                var answer = (result.length > 0) ? true : false;
                resolve(answer);
            });
        });
    }

    const createDatabase = (dbName) => {
        console.log(`Attempting to create '${dbName}' database`);
        return new Promise((resolve, reject) => {
            con1.query(`CREATE DATABASE ${dbName}`, function (error, result) {
                // Check for errors while looking up
                if (error) throw error;
                // Successfully created a database
                console.log(`Successfully created '${dbName}' database`);
                resolve(true);
            });
        });
    }

    const createTable = (tblName) => {
        console.log(`Attempting to create '${tblName}' table`);
        return new Promise((resolve, reject) => {
            con2.query(`
                CREATE TABLE ${tblName} (
                    product_name VARCHAR(255),
                    sku VARCHAR(255),
                    advertiser_name VARCHAR(255),
                    advertiser_id INTEGER
                )`, function (error, result) {
                    // Check for errors while looking up
                    if (error) throw error;
                    // Successfully created a database
                    console.log(`Successfully created '${tblName}' table`);
                    resolve(true);
                }
            );
        });
    };

    const writeSeedDataNew = (data, tblName) => {
        // Loop through data array and each item in it
        console.log(`Attempting to write all data into '${tblName}' table`);
        const dataEntriesCount = data.length;
        let adID = 0;
        let loopIndex = 1;
        const errorData = [];
        const advertiserKeys = {};
        const bar = new ProgressBar('Running mySQL queries [:bar] (Count - :current/:total) (Time Remaining - :etas)', {
            total: (dataEntriesCount - 1),
            complete: '\u001b[42m \u001b[0m',
            incomplete: '\u001b[41m \u001b[0m',
            width: 20,
        });

        const checkFinishLoop = () => {
            if (loopIndex < dataEntriesCount) {
                writeLoop();
            } else {
                // Check for any errorData
                if (errorData.length) {
                    console.log(`One or more errors found in data, see '${config.folders.errors}/corruptData.txt'.  All other data successfully writen.`);
                    const errorFile = fs.createWriteStream(`${config.folders.errors}/corruptData.txt`);
                    errorFile.on('error', (error) => { throw error });
                    errorData.forEach((value) => {
                        errorFile.write(value.join(', ') + '\n');
                    });
                    errorFile.end();
                    console.log("Run 'npm start' in terminal to start local React application");
                    process.exitCode = 1;
                } else {
                    // Successfully added all data
                    console.log(`All data successfully writen into '${tblName}'`);
                    console.log("Run 'npm start' in terminal to start local React application");
                    process.exitCode = 1;
                }
            }
        }

        const writeLoop = () => {
            const currentData = data[loopIndex];
            loopIndex += 1;
            bar.tick();
            // Check requirements
            if (currentData.length !== 3) {
                // Not the right size!
                errorData.push(currentData);
                checkFinishLoop();
            } else {
                let useID;
                // Loopup Advertiser ID
                if (advertiserKeys[currentData[2]]){ // Advertiser has already been accounted for
                    useID = advertiserKeys[currentData[2]];
                } else {
                    adID += 1;
                    advertiserKeys[currentData[2]] = adID;
                    useID = adID;
                }
                // 0: Product Name, 1: SKU, 2: Advertiser
                con2.query(`INSERT INTO ${tblName} (product_name, sku, advertiser_name, advertiser_id) VALUES ('${currentData[0]}', '${currentData[1]}', '${currentData[2]}', '${useID}')`,
                (error, result) => {
                    // Check for errors while looking up
                    if (error) throw error;
                    checkFinishLoop();
                })
            }
        }
        writeLoop();
    };

    const runTableCreation = (tblName) => {
        // Create table
        createTable(tblName).then(()=>{
            // Get data
            readSeedData().then(data => {
                // Set data
                writeSeedDataNew(data, tblName);
            });
        })
    }

    // Connect into SQL database
    con1.connect((error) => {
        // Check for errors connecting
        if (error) throw error;
    
        // Successfully connected 
        console.log('Connected to localhost mySQL server');

        // Check to see if database exists
        checkDatabase(config.mysql.database).then( database => {
            if (!database){ // Doesn't exist, create it
                console.log(`'${config.mysql.database}' database does not exist`);
                // Build database
                createDatabase(config.mysql.database).then(
                    // Check if table exists
                    checkTable(config.mysql.table).then( table => {
                        if (!table) { // Table doesn't exist
                            console.log(`'${config.mysql.table}' table doesn't exist`);
                            runTableCreation(config.mysql.table);
                        } else { // Table exists
                            console.log(`'${config.mysql.table}' table already exists`);
                            // Get data
                            readSeedData().then(data => {
                                // Set data
                                writeSeedDataNew(data, tblName);
                            });
                        }
                    })
                );
            } else { // Database exists
                console.log(`'${config.mysql.database}' database already exists`);
                // Check if table exists
                checkTable(config.mysql.table).then( table => {
                    if (!table) { // Table doesn't exist
                        console.log(`'${config.mysql.table}' table doesn't exist`);
                        runTableCreation(config.mysql.table);
                    } else { // Table exists
                        console.log(`'${config.mysql.table}' table already exists`);
                        // Get data
                        readSeedData().then(data => {
                            // Set data
                            writeSeedDataNew(data, config.mysql.table);
                        });
                    }
                })
            }
        });
    });
}

module.exports = setupDatabase;
