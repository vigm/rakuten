const fs = require('fs');
const config = require('../config');

const checkFolders = () => {
    return new Promise((resolve, reject) => {
        console.log('Checking for required directories');
        // Folders to check
        const folderDatabase = config.folders.database;
        const folderSeed = config.folders.seed;
        const folderDecompressed = config.folders.decompressed;
        const folderErrors = config.folders.errors;

        const folderCheck = (folder) => {
            // Does the folder exist
            if (fs.existsSync(folder)) {
                console.log(`'${folder}' folder already exists`);
                return;
            } else {
                console.log(`'${folder}' folder doesn't exist, creating directory`);
                fs.mkdirSync(folder);
            }
        }

        // Build each folder as needed
        folderCheck(folderDatabase);
        folderCheck(folderSeed);
        folderCheck(folderDecompressed);
        folderCheck(folderErrors);

        resolve('Finished building folders');
    })
}

module.exports = checkFolders;