const config = {
    folders: {
        database: 'database',
        seed: 'database/seed',
        decompressed: 'database/seed/decompressed',
        errors: 'database/seed/errors',
    },
    files: {
        compressed: 'products.tar.gz',
        products: 'products.csv',
        advertisers: 'advertisers.txt',
    },
    seedURL: 'https://s3.amazonaws.com/rm-rant-interviewing/products.tar.gz',
    mysql: {
        host: '127.0.0.1',
        user: 'root',
        password: 'password',
        database: 'rakuten',
        table: 'products',
    },
}

module.exports = config;