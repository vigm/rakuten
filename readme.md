Full-stack Developer Assignment
Frontend Create a single page application with either React, Angular/JS, Backbone, or Vue. The application will primarily consist of at least one custom built component. The component then renders the list as a table. Populate the table with the results of the API endpoint described below.

Backend Using any server side language and any MVC framework/architecture, create a new backend service with two resources: products and their corresponding advertisers. Using any relational database, you will setup a schema that includes the following attributes:

Product name
Product SKU
associated advertiser id
Advertiser name
The ID, name and SKU should be unique for each product. We have created a seed file for you to download, process and insert into your database via a script of your own creation. The file lives at https://s3.amazonaws.com/rm-rant-interviewing/products.tar.gz and should not be included in the repo. Your script will be submitted as part of the assignment and will be run by us to validate the homework. Also as part of this, we'd like you to describe an efficient approach to inserting the file. How would this approach scale?
------------------------------------------------

# Homework Notes:

## To Run:
*  Uses mySQL to create a local database.  Use the config.js found in ```./server/config``` to set up basic information
*  The first command to run is ```npm install``` in terminal, this will add all required packages
*  Make sure you are using node version 8 (if using nvm, ```nvm use 8```)
*  Run ```npm run setup```, this will build out the database and folder structure as needed (Might take ~400s to populate server)
*  Run ```npm start```, this will start up the local server with React and allow you to access data from the server
*  View project on localhost:3600

## Slight Issues/Concerns:
*  "The ID, name and SKU should be unique for each product."
    *  Not clear with what "Name".  Could be Product or Advertiser Name.
    *  Not feasible with the seed data given...
        *  There are lots of products with the same name, and advertiser, while having a unique SKU
*  Several entries in the CSV have an extra column in them ```,,``` maybe a typo or something those are ignored and added into database/errors

## Homework Question
```describe an efficient approach to inserting the file. How would this approach scale?```
Currently my script runs line by line to validate the CSV before sending off the SQL command due to several inconsistencies/errors in the CSV. I catch and write any problems into a separate folder.  With additional time I would have split this step away from the SQL commands and have it be a two step process, validate the CSV, then if valid, use the mass import from CSV mySQL function.

Users could use a front end portal to upload a CSV and test it at the same time, then if the test is passed they will be asked which database/table they will want to import it into (the same thing as this project, just with a GUI for the user).