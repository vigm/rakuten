const con = require('./connection');
const config = require ('../../server/config.js');

const limit = 50;

const Products = {
    getAllProducts: (page = 1, count, callback) => {
        console.log(`getAllProducts on page ${page}`);
        if (count) {
            return con.query(`SELECT COUNT(*) FROM ${config.mysql.table}`, callback);
        }
        return con.query(`Select * from ${config.mysql.table} ORDER BY product_name LIMIT ${limit} OFFSET ${page * limit}`, callback);
    },
    getProductsByProductName: function(productName, page = 1, count, callback) {
        console.log(`getProductsByProductName: ${productName} on page ${page}`);
        if (count) {
            return con.query(`SELECT COUNT(*) FROM ${config.mysql.table} WHERE product_name='${productName}'`, callback);
        }
        return con.query(`
            (SELECT * FROM ${config.mysql.table} 
            WHERE product_name='${productName}' 
            ORDER BY advertiser_id) 
            LIMIT ${page * limit},${limit}`, [productName], callback);
    },
    // getProductsByProductName: function(productName, callback) {
    //     console.log('getProductsByProductName');
    //     return con.query(`select * from ${config.mysql.table} where product_name=${productName}`, [productName], callback);
    // },
    getProductsByProductSKU: function(advertiserSKU, page = 1, count, callback) {
        console.log(`getProductsByProductSKU: ${advertiserSKU} on page ${page}`);
        if (count) {
            return con.query(`SELECT COUNT(*) FROM ${config.mysql.table} WHERE sku='${advertiserSKU}'`, callback);
        }
        return con.query(`
            SELECT * FROM ${config.mysql.table} 
            WHERE sku='${advertiserSKU}' 
            ORDER BY product_name 
            LIMIT ${limit} 
            OFFSET ${page * limit}`, [advertiserSKU], callback);
    },
    // getProductsByProductSKU: function(sku, callback) {
    //     console.log('getProductsByProductSKU');
    //     return con.query(`select * from ${config.mysql.table} where sku=${sku}`, [sku], callback);
    // },
    getProductsByAdvertiserName: function(advertiserName, page = 1, count, callback) {
        console.log(`getProductsByAdvertiserName: ${advertiserName} on page ${page}`);
        if (count) {
            return con.query(`SELECT COUNT(*) FROM ${config.mysql.table} WHERE advertiser_name='${advertiserName}'`, callback);
        }
        return con.query(`
            SELECT * FROM ${config.mysql.table} 
            WHERE advertiser_name='${advertiserName}' 
            ORDER BY product_name  
            LIMIT ${limit} 
            OFFSET ${page * limit}`, [advertiserName], callback);
    },
    // getProductsByAdvertiserName: function(advertiserName, callback) {
    //     console.log('getProductsByAdvertiserName');
    //     return con.query(`select * from ${config.mysql.table} where advertiser_name=${advertiserName}`, [advertiserName], callback);
    // },
    getProductsByAdvertiserID: function(advertiserID, page = 1, count, callback) {
        console.log(`getProductsByAdvertiserID: ${advertiserID} on page ${page}`);
        if (count) {
            return con.query(`SELECT COUNT(*) FROM ${config.mysql.table} WHERE advertiser_id=${advertiserID}`, callback);
        }
        return con.query(`
            SELECT * FROM ${config.mysql.table} 
            WHERE advertiser_id=${advertiserID} 
            ORDER BY product_name 
            LIMIT ${limit} 
            OFFSET ${page * limit}`, [advertiserID], callback);
    }
};

module.exports = Products;
