const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const packageJson = require('./package.json');
const webpack = require('webpack');

const ENV = process.env.NODE_ENV || 'development';
module.exports = {
    entry: {
        [`${packageJson.name}.${packageJson.version}`]: ['./lib/js/index.js']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'less-loader']
                })
            }
        ]
    },
    output: {
        filename: 'js/[name].min.js',
        path: path.join(__dirname, 'dist')
    },
    plugins: [
        new ExtractTextPlugin('css/[name].min.css'),
        new HtmlWebpackPlugin({
            template: 'lib/local/index.html'
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(ENV)
        }),
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        port: 3600
    }
};
